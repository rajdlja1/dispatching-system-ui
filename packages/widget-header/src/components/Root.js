import React from 'react';
import { IntlProvider, addLocaleData } from 'react-intl';

import cs from 'react-intl/locale-data/cs';
import sk from 'react-intl/locale-data/sk';

import App from './App';

import messages_cs from '../../../../react/translations/cs.json';
import messages_en from '../../../../react/translations/en.json';

const messages = {
	cs: messages_cs,
	en: messages_en,
};

const language = navigator.language.split(/[-_]/)[0]; // language without region code

addLocaleData([...cs, ...sk]);

const Root = () => (
	<IntlProvider locale={language} messages={messages[language]}>
		<App />
	</IntlProvider>
);

export default Root;
