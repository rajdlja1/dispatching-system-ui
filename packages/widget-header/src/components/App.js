import React from 'react';
import { FormattedMessage } from 'react-intl';

import logo from './logo.png';

const Root = () => (
	<div className="container">
		<img className="img-fluid" src={logo} />
		<nav className="navbar navbar-default">
			<div className="container-fluid">
				<div className="navbar-header" />
			</div>
		</nav>
	</div>
);

export default Root;
