import React from 'react';
import { FormattedMessage, FormattedTime } from 'react-intl';
import { getFormattedDate } from 'library-ds';
import renderVehicle from '../components/Vehicle';
import { LIFERAY_HOST_PUBLIC_SPACE, RideViewModeEnum, TRAINSET_EDITOR_PAGE } from '../constants';

function renderRide(key, ride, rideViewMode) {
	const rideId = ride.id;
	const vehicles = [];

	const departure = `${ride.routeSection.departureTime.substr(0, 10)}-${ride.routeSection.departureTime.substr(11, 5)}`;
	const arrival = `${ride.routeSection.arrivalTime.substr(0, 10)}-${ride.routeSection.arrivalTime.substr(11, 5)}`;

	const link = `${LIFERAY_HOST_PUBLIC_SPACE}/${TRAINSET_EDITOR_PAGE}?id=${ride.id}&od=${departure}&do=${arrival}&depotId=${ride.routeSection.from.id}`;

	if (ride.vehicles != null) {
		ride.vehicles.forEach(function(vehicle) {
			if (vehicle != null && vehicle.description != null) {
				vehicles.push(renderVehicle(vehicle, rideId));
			}
		});
	}

	return (
		<tr key={ride.id} className="ride">
			<td />
			{rideViewMode === RideViewModeEnum.TRAIN_OVERVIEW ? <td /> : ''}
			<td>
				{vehicles.length > 0 ? (
					vehicles.map(t => t).reduce((prev, curr) => [prev, ' ', curr])
				) : (
					<FormattedMessage id="label.notHaveConfiguration" />
				)}
			</td>
			<td>
				{ride.driver !== null ? (
					`${ride.driver.firstname} ${ride.driver.lastname}`
				) : (
					<FormattedMessage id="label.notAssigned" />
				)}
			</td>
			<td>{ride.driver !== null ? ride.driver.phoneNunber : ''}</td>
			<td>{ride.routeSection.from.location}</td>
			<td>
				<FormattedTime value={ride.routeSection.departureTime} />
			</td>
			<td>{ride.routeSection.to.location}</td>
			<td>
				<FormattedTime value={ride.routeSection.arrivalTime} />
			</td>
			<td>
				<div className="btn-toolbar" role="toolbar" label="Toolbar with button groups">
					<div className="btn-group mr-2" role="group" label="First group">
						<a href={link} className="btn btn-info route-button">
							<FormattedMessage id="label.button.editRouteSection" /></a>
					</div>
					<div className="btn-group mr-2" role="group" label="First group" />
				</div>
			</td>
		</tr>
	);
}

export default renderRide;
