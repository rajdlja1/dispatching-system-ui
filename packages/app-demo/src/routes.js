import loadHero from 'widget-hero';
import loadContent from 'widget-content';
import loadTrainOverview from 'widget-train-overview';
import loadTrainsetOverview from 'widget-trainset-overview';
import loadRouteOverview from 'widget-route-overview';
import loadRouteDetail from 'widget-route-detail';
import loadTrainsetDetail from 'widget-trainset-detail';
import loadTrainsetEditor from 'widget-trainset-editor';
import loadHeader from 'widget-header';

export default [
	{
		path: 'hero',
		getComponent(done) {
			loadHero(mod => done(mod.default));
		},
	},
	{
		path: 'content',
		getComponent(done) {
			loadContent(mod => done(mod.default));
		},
	},
	{
		path: 'train-overview',
		getComponent(done) {
			loadTrainOverview(mod => done(mod.default));
		},
	},
	{
		path: 'train-overview2',
		getComponent(done) {
			loadTrainOverview(mod => done(mod.default));
		},
	},
	{
		path: 'trainset-overview',
		getComponent(done) {
			loadTrainsetOverview(mod => done(mod.default));
		},
	},
	{
		path: 'route-overview',
		getComponent(done) {
			loadRouteOverview(mod => done(mod.default));
		},
	},
	{
		path: 'route-detail',
		getComponent(done) {
			loadRouteDetail(mod => done(mod.default));
		},
	},
	{
		path: 'trainset-detail',
		getComponent(done) {
			loadTrainsetDetail(mod => done(mod.default));
		},
	},
	{
		path: 'trainset-editor',
		getComponent: done => {
			loadTrainsetEditor(mod => done(mod.default));
		},
	},
	{
		path: 'header',
		getComponent(done) {
			loadHeader(mod => done(mod.default));
		},
	},
];
