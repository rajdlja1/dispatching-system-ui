/* TODO Change hardcoded URLs */

export const REQUEST_AVAILABLE_VEHICLES = 'REQUEST_AVAILABLE_VEHICLES';
export const RECEIVE_AVAILABLE_VEHICLES = 'RECEIVE_AVAILABLE_VEHICLES';
export const REQUEST_RIDE = 'REQUEST_RIDE';
export const RECEIVE_RIDE = 'RECEIVE_RIDE';
export const REQUEST_SAVE_RIDE_CONFIGURATION = 'REQUEST_SAVE_RIDE_CONFIGURATION';
export const RESPONSE_SAVE_RIDE_CONFIGURATION = 'RESPONSE_SAVE_RIDE_CONFIGURATION';
export const SAVE_TRAINSET_CONFIGURATION = 'SAVE_TRAINSET_CONFIGURATION';

/* Available Vehicles Loading Actions */

export const fetchAvailableVehicles = (since, until, id) => {
	return dispatch => {
		dispatch(requestAvailableVehicles(since, until, id));
		return fetch(`http://localhost:8080/depa/${id}/dostupnost?od=${since}&do=${until}`)
			.then(response => response.json())
			.then(json => dispatch(receiveAvailableVehicles(json)));
	};
};

export const requestAvailableVehicles = (since, until, id) => ({
	type: REQUEST_AVAILABLE_VEHICLES,
	since,
	until,
	depotId: id,
});

export const receiveAvailableVehicles = json => ({
	type: RECEIVE_AVAILABLE_VEHICLES,
	availableVehicles: json,
	receivedAt: Date.now(),
});

/* Ride Loading Actions */

export const fetchRide = id => {
	return dispatch => {
		dispatch(requestRide(id));
		return fetch(`http://localhost:8080/jizdy/${id}`)
			.then(response => response.json())
			.then(json => dispatch(receiveRide(json)));
	};
};

export const requestRide = id => ({
	type: REQUEST_RIDE,
	rideId: id,
});

export const receiveRide = json => ({
	type: RECEIVE_RIDE,
	ride: json,
	receivedAt: Date.now(),
});

/* Ride Config Update Actions */

export const saveRideConfiguration = (id, ride) => {
	return dispatch => {
		dispatch(requestSaveRideConfiguration(ride));
		return fetch(`http://localhost:8080/jizdy/${id}/konfigurace`, {
			method: 'PUT',
			body: JSON.stringify(ride),
			headers: {
				'Content-Type': 'application/json',
			},
		})
			.then(response => response.json())
			.then(json => dispatch(responseSaveRideConfiguration(json)));
	};
};

export const requestSaveRideConfiguration = ride => ({
	type: REQUEST_SAVE_RIDE_CONFIGURATION,
	ride,
});

export const responseSaveRideConfiguration = ride => ({
	type: RESPONSE_SAVE_RIDE_CONFIGURATION,
	ride,
	receivedAt: Date.now(),
});
