import { FormattedMessage } from 'react-intl';
import Route from './Route';

const React = require('react');

function renderRouteTable(routes) {
	const rows = [];
	Array.prototype.forEach.call(routes, route => {
		rows.push(<Route key={route.id} route={route} />);
	});

	return (
		<table className="table table-striped">
			<thead>
				<tr>
					<th>
						<FormattedMessage id="label.route" />
					</th>
					<th>
						<FormattedMessage id="label.trainSet" />
					</th>
					<th>
						<FormattedMessage id="label.cars" />
					</th>
					<th>
						<FormattedMessage id="label.driver" />
					</th>
					<th>
						<FormattedMessage id="label.phoneNumber" />
					</th>
					<th>
						<FormattedMessage id="label.stationOfDeparture" />
					</th>
					<th>
						<FormattedMessage id="label.timeOfDeparture" />
					</th>
					<th>
						<FormattedMessage id="label.stationOfArrival" />
					</th>
					<th>
						<FormattedMessage id="label.timeOfArrival" />
					</th>
					<th>
						<FormattedMessage id="label.actions" />
					</th>
				</tr>
			</thead>
			{rows}
		</table>
	);
}

export default renderRouteTable;
