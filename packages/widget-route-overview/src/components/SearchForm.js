import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage, FormattedTime, injectIntl} from 'react-intl';
import { getFormattedDate, Datepicker } from 'library-ds';
import { invalidateForm, changeCheckBox, reloadItemsIfNeeded } from '../actions';

const React = require('react');

const DaysEnum = Object.freeze({
	ON_THE_WAY: 'ON_THE_WAY',
	ARRIVED: 'ARRIVED',
	WILL_DEPARTURE: 'WILL_DEPARTURE',
	TODAY: 'TODAY',
	NOTHING_SELECTED: 'NOTHING_SELECTED',
});

class SearchForm extends React.Component {
	static ON_THE_WAY = 'NACESTE';
	static ARRIVAL = 'PRIJEZD';
	static DEPARTURE = 'ODJEZD';

	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleCheckboxState = val => {
		if (val === this.props.checkboxValue) {
			this.props.dispatch(changeCheckBox(DaysEnum.NOTHING_SELECTED));
		} else {
			this.props.dispatch(changeCheckBox(val));
		}
	};

	handleDatePicker(datePicker) {
		this.setState({ date: datePicker });
	}

	handleSubmit(event) {
		event.preventDefault();

		let time = SearchForm.DEPARTURE;
		const startOfDate = new Date();
		startOfDate.setHours(0, 0, 0, 0);
		const endOfDate = new Date();
		endOfDate.setHours(23, 59, 0, 0);
		const now = new Date();

		// Default Option is TODAY
		let since = getFormattedDate(startOfDate);
		let until = getFormattedDate(endOfDate);

		// Today Option
		if (this.props.checkboxValue === DaysEnum.TODAY) {
			since = getFormattedDate(startOfDate);
			until = getFormattedDate(endOfDate);
		} else if (this.props.checkboxValue === DaysEnum.ON_THE_WAY) {
			// Now
			since = getFormattedDate(now);
			until = getFormattedDate(now);
			time = SearchForm.ON_THE_WAY;
		} else if (this.props.checkboxValue === DaysEnum.ARRIVED) {
			// Arrived Today
			since = getFormattedDate(startOfDate);
			until = getFormattedDate(now);
			time = SearchForm.ARRIVAL;
		} else if (this.props.checkboxValue === DaysEnum.WILL_DEPARTURE) {
			// Will Departure Today
			since = getFormattedDate(now);
			until = getFormattedDate(endOfDate);
			time = SearchForm.DEPARTURE;
		} else if (this.props.selectedDate != null) {
			// Check date from datepicker
			const startOfSubmDate = new Date(this.props.selectedDate.getTime());
			startOfSubmDate.setHours(0, 0, 0, 0);
			const endOfSubmDate = new Date(this.props.selectedDate.getTime());
			endOfSubmDate.setHours(23, 59, 0, 0);
			since = getFormattedDate(startOfSubmDate);
			until = getFormattedDate(endOfSubmDate);
		}

		this.props.dispatch(invalidateForm());
		this.props.dispatch(reloadItemsIfNeeded(since, until, time));
	}

	render() {
		const { intl } = this.props;
		const { lastUpdated } = this.props;
		return (
			<div className="panel-group">
				<div className="panel panel-default">
					<div className="panel-body">
						<form onSubmit={this.handleSubmit} className="searchForm">
							<div className="form-group">
								<Datepicker />
							</div>

							<div className="form-group">
								<span className="form-check form-check-inline">
									<input
										className="form-check-input"
										type="checkbox"
										id="todayCheckbox"
										value="todayOption"
										checked={this.props.checkboxValue === DaysEnum.TODAY}
										onChange={() => this.handleCheckboxState(DaysEnum.TODAY)}
									/>
									<label className="form-check-label" htmlFor="todayCheckbox">
										<FormattedMessage id="label.form.today" />
									</label>
								</span>
								<span className="form-check form-check-inline">
									<input
										className="form-check-input"
										type="checkbox"
										id="nowCheckbox"
										value="nowOption"
										checked={this.props.checkboxValue === DaysEnum.ON_THE_WAY}
										onChange={() => this.handleCheckboxState(DaysEnum.ON_THE_WAY)}
									/>
									<label className="form-check-label" htmlFor="nowCheckbox">
										<FormattedMessage id="label.form.onTheWay" />
									</label>
								</span>
								<span className="form-check form-check-inline">
									<input
										className="form-check-input"
										type="checkbox"
										id="arrivedCheckbox"
										value="arrivedOption"
										checked={this.props.checkboxValue === DaysEnum.ARRIVED}
										onChange={() => this.handleCheckboxState(DaysEnum.ARRIVED)}
									/>
									<label className="form-check-label" htmlFor="arrivedCheckbox">
										<FormattedMessage id="label.form.arrived" />
									</label>
								</span>
								<span className="form-check form-check-inline">
									<input
										className="form-check-input"
										type="checkbox"
										id="willDepartureCheckbox"
										value="willDepartureOption"
										checked={this.props.checkboxValue === DaysEnum.WILL_DEPARTURE}
										onChange={() => this.handleCheckboxState(DaysEnum.WILL_DEPARTURE)}
									/>
									<label className="form-check-label" htmlFor="willDepartureCheckbox">
										<FormattedMessage id="label.form.willDeparture" />
									</label>
								</span>

								<input
									type="submit"
									value={intl.formatMessage({ id: 'label.form.submit.show' })}
									className="btn btn-primary"
								/>
							</div>
						</form>
						{lastUpdated && (
							<p>
								<FormattedMessage id="label.lastUpdatedAt" />{' '}
								<FormattedTime value={new Date(lastUpdated)} />.
							</p>
						)}
					</div>
				</div>
			</div>
		);
	}
}

SearchForm.propTypes = {
	checkboxValue: PropTypes.string,
	dispatch: PropTypes.func,
	handleFilterTrainsets: PropTypes.func,
	lastUpdated: PropTypes.number,
	selectedDate: PropTypes.instanceOf(Date),
};

const mapStateToProps = state => {
	const { filteredRoutes } = state;
	const { lastUpdated } = filteredRoutes.routes || {};
	return {
		checkboxValue: state.changedCheckboxValue,
		selectedDate: state.selectedDate,
		lastUpdated,
	};
};

export default connect(mapStateToProps)(injectIntl(SearchForm));
