import { combineReducers } from 'redux';
import {
	REQUEST_TRAINSET,
	RECEIVE_TRAINSET,
	SHOW_ROUTE_SECTIONS_CHANGED,
	SELECT_FROM_DATE,
	SELECT_TO_DATE,
	CHECKBOX_CHANGE,
	INVALIDATE_FORM,
} from '../actions';

const selectedToDate = (state = new Date(), action) => {
	switch (action.type) {
		case SELECT_TO_DATE:
			return action.dateTime;
		default:
			return state;
	}
};

const selectedFromDate = (state = new Date(), action) => {
	switch (action.type) {
		case SELECT_FROM_DATE:
			return action.dateTime;
		default:
			return state;
	}
};

const todayCheckBox = (state = false, action) => {
	switch (action.type) {
		case CHECKBOX_CHANGE:
			return !state;
		default:
			return state;
	}
};

const trainsetMan = (
	state = {
		isFetching: false,
		didInvalidate: false,
		item: null,
	},
	action
) => {
	switch (action.type) {
		case INVALIDATE_FORM:
			return {
				...state,
				didInvalidate: true,
			};
		case REQUEST_TRAINSET:
			return {
				...state,
				isFetching: true,
				didInvalidate: false,
			};
		case RECEIVE_TRAINSET:
			console.log('test');
			console.log(action.trainset);
			action.trainset.trains.forEach(function(train) {
				train.showRouteSections = false;
			});

			return {
				...state,
				isFetching: false,
				didInvalidate: false,
				item: action.trainset,
				lastUpdated: action.receivedAt,
			};
		default:
			return state;
	}
};

const filteredTrainset = (state = {}, action) => {
	switch (action.type) {
		case RECEIVE_TRAINSET:
		case REQUEST_TRAINSET:
		case INVALIDATE_FORM:
			return {
				...state,
				trainset: trainsetMan(state.trainset, action),
			};
		case SHOW_ROUTE_SECTIONS_CHANGED:
			let i;
			let value = false;
			for (i = 0; i < state.trainset.item.trains.length; i++) {
				if (state.trainset.item.trains[i].id === action.train.id) {
					value = state.trainset.item.trains[i].showRouteSections;
					break;
				}
			}

			return {
				...state,
				trainset: {
					...state.trainset,
					item: {
						...state.trainset.item,
						trains: state.trainset.item.trains.map(
							(e, j) => (j === i ? { ...e, showRouteSections: !value } : e)
						),
					},
				},
			};
		default:
			return state;
	}
};

const rootReducer = combineReducers({
	filteredTrainset,
	selectedToDate,
	selectedFromDate,
	todayCheckBox,
});

export default rootReducer;
