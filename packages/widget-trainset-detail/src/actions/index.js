import { API_HOST, API_TRAINSET_PREFIX } from 'library-ds';

export const REQUEST_TRAINSET = 'REQUEST_TRAINSET';
export const RECEIVE_TRAINSET = 'RECEIVE_TRAINSET';
export const SHOW_ROUTE_SECTIONS_CHANGED = 'SHOW_ROUTE_SECTIONS_CHANGED';
export const SELECT_FROM_DATE = 'SELECT_FROM_DATE';
export const SELECT_TO_DATE = 'SELECT_TO_DATE';
export const INVALIDATE_FORM = 'INVALIDATE_FORM';
export const CHECKBOX_CHANGE = 'CHECKBOX_CHANGE';

export const showRouteSectionsChanged = train => ({
	type: SHOW_ROUTE_SECTIONS_CHANGED,
	train,
});

export const selectFromDate = dateTime => ({
	type: SELECT_FROM_DATE,
	dateTime,
});

export const checkBoxChanged = () => ({
	type: CHECKBOX_CHANGE,
});

export const selectToDate = dateTime => ({
	type: SELECT_TO_DATE,
	dateTime,
});

export const fetchTrainset = (trainsetId) => {
	const link = `${API_HOST}/${API_TRAINSET_PREFIX}/${trainsetId}`;
	return dispatch => {
		dispatch(requestTrainset());
		return fetch(link)
			.then(response => response.json())
			.then(json => dispatch(receiveTrainset(json)));
	};
};

export const requestTrainset = () => ({
	type: REQUEST_TRAINSET,
});

export const receiveTrainset = json => ({
	type: RECEIVE_TRAINSET,
	trainset: json,
	receivedAt: Date.now(),
});

export const invalidateForm = () => ({
	type: INVALIDATE_FORM,
});

const shouldReloadTrainset = state => {
	const posts = state.filteredPosts;
	if (!posts) {
		return true;
	}
	if (posts.isFetching) {
		return false;
	}
	return posts.didInvalidate;
};

export const reloadTrainsetIfNeeded = (since, until, time, id) => (dispatch, getState) => {
	if (shouldReloadTrainset(getState())) {
		return dispatch(reloadItems(since, until, time, id));
	}
};

const reloadItems = (since, until, time, trainsetId) => dispatch => {
	const link = `${API_HOST}/${API_TRAINSET_PREFIX}/${trainsetId}/hledej?od=${since}&do=${until}&cas=${time}`;
	dispatch(requestTrainset());
	return fetch(link)
		.then(response => response.json())
		.then(json => dispatch(receiveTrainset(json)));
};
