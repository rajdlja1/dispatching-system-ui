import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { FormattedMessage } from 'react-intl';
import renderTrainTable from './TrainTable';
import SearchForm from './SearchForm';

const React = require('react');

function renderTrainset(trainset) {
	return (
		<div>
			<div className="jumbotron">
				<div className="row">
					<div className="col-sm-8">
						<h1>
							<FormattedMessage id="label.trainSet.detail" />
						</h1>
					</div>
					<div className="col-sm-4">
						<h2>{trainset.name}</h2>
					</div>
				</div>
			</div>
			<MuiThemeProvider>
				<SearchForm id={trainset.id} />
			</MuiThemeProvider>
			{renderTrainTable(trainset)}
		</div>
	);
}

export default renderTrainset;
