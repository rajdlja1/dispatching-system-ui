import { API_HOST, API_ROUTE_PREFIX, API_TRAINSET_PREFIX } from 'library-ds';

export const REQUEST_ROUTE = 'REQUEST_ROUTE';
export const RECEIVE_ROUTE = 'RECEIVE_ROUTE';
export const SHOW_ROUTE_SECTIONS_CHANGED = 'SHOW_ROUTE_SECTIONS_CHANGED';
export const SELECT_FROM_DATE = 'SELECT_FROM_DATE';
export const SELECT_TO_DATE = 'SELECT_TO_DATE';
export const INVALIDATE_FORM = 'INVALIDATE_FORM';
export const CHECKBOX_CHANGE = 'CHECKBOX_CHANGE';

export const showRouteSectionsChanged = train => ({
	type: SHOW_ROUTE_SECTIONS_CHANGED,
	train,
});

export const selectFromDate = dateTime => ({
	type: SELECT_FROM_DATE,
	dateTime,
});

export const checkBoxChanged = () => ({
	type: CHECKBOX_CHANGE,
});

export const selectToDate = dateTime => ({
	type: SELECT_TO_DATE,
	dateTime,
});

export const fetchRoute = (routeId) => {
	const link = `${API_HOST}/${API_ROUTE_PREFIX}/${routeId}`;
	return dispatch => {
		dispatch(requestRoute());
		return fetch(link)
			.then(response => response.json())
			.then(json => dispatch(receiveRoute(json)));
	};
};

export const requestRoute = () => ({
	type: REQUEST_ROUTE,
});

export const receiveRoute = json => ({
	type: RECEIVE_ROUTE,
	trainset: json,
	receivedAt: Date.now(),
});

export const invalidateForm = () => ({
	type: INVALIDATE_FORM,
});

const shouldReloadRoute = state => {
	const posts = state.filteredPosts;
	if (!posts) {
		return true;
	}
	if (posts.isFetching) {
		return false;
	}
	return posts.didInvalidate;
};

export const reloadRouteIfNeeded = (since, until, time, id) => (dispatch, getState) => {
	if (shouldReloadRoute(getState())) {
		return dispatch(reloadItems(since, until, time, id));
	}
};

const reloadItems = (since, until, time, routeId) => dispatch => {
	const link = `${API_HOST}/${API_ROUTE_PREFIX}/${routeId}/hledej?od=${since}&do=${until}&cas=${time}`;
	dispatch(requestRoute());
	return fetch(link)
		.then(response => response.json())
		.then(json => dispatch(receiveRoute(json)));
};
