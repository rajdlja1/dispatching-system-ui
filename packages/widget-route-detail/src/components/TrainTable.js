import React from 'react';
import { FormattedMessage } from 'react-intl';
import Train from './Train';

function renderTrainTable(obj) {
	const rows = [];

	Array.prototype.forEach.call(obj.trains, train => {
		rows.push(<Train key={train.id} train={train} />);
	});

	return (
		<div>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>
							<FormattedMessage id="label.trainSet" />
						</th>
						<th>
							<FormattedMessage id="label.cars" />
						</th>
						<th>
							<FormattedMessage id="label.driver" />
						</th>
						<th>
							<FormattedMessage id="label.phoneNumber" />
						</th>
						<th>
							<FormattedMessage id="label.stationOfDeparture" />
						</th>
						<th>
							<FormattedMessage id="label.timeOfDeparture" />
						</th>
						<th>
							<FormattedMessage id="label.stationOfArrival" />
						</th>
						<th>
							<FormattedMessage id="label.timeOfArrival" />
						</th>
						<th>
							<FormattedMessage id="label.actions" />
						</th>
					</tr>
				</thead>
				{rows}
			</table>
		</div>
	);
}

export default renderTrainTable;
