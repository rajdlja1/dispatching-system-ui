import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { FormattedMessage } from 'react-intl';
import renderTrainTable from './TrainTable';
import SearchForm from './SearchForm';

function renderRoute(route) {
	return (
		<div>
			<div className="jumbotron">
				<div className="row">
					<div className="col-sm-8">
						<h1>
							<FormattedMessage id="label.route.detail" /> {route.name}
						</h1>
					</div>
					<div className="col-sm-4">
						<h2>
							{route.from.location} - {route.to.location}
						</h2>
					</div>
				</div>
			</div>
			<MuiThemeProvider>
				<SearchForm routeId={route.id} />
			</MuiThemeProvider>
			{renderTrainTable(route)}
		</div>
	);
}

export default renderRoute;
