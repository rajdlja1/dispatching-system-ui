import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { fetchRoute } from '../actions';
import renderRoute from '../components/RouteDetail';

class App extends Component {
	static propTypes = {
		dispatch: PropTypes.func.isRequired,
		id: PropTypes.number.isRequired,
		isFetching: PropTypes.bool.isRequired,
		route: PropTypes.shape(),
	};

	componentDidMount() {
		const { dispatch } = this.props;
		dispatch(fetchRoute(this.props.id));
	}

	render() {
		const { route, isFetching } = this.props;
		return (
			<div className="container">
				{isFetching ? (
					<h2>
						<FormattedMessage id="label.loading" />
					</h2>
				) : (
					<div style={{ opacity: isFetching ? 0.5 : 1 }}>{renderRoute(route)}</div>
				)}
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { filteredRoute } = state;
	const { isFetching, lastUpdated, item: route } = filteredRoute.route || {
		isFetching: true,
		item: null,
	};

	return {
		route,
		isFetching,
		lastUpdated,
	};
};

export default connect(mapStateToProps)(App);
