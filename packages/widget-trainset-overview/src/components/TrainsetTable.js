/* eslint-disable react/prefer-stateless-function */
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import Trainset from '../components/Trainset';

const React = require('react');

class TrainsetTable extends React.Component {
	render() {
		const rows = [];
		Array.prototype.forEach.call(this.props.trainsets, trainset => {
			rows.push(<Trainset key={trainset.id} trainset={trainset} />);
		});

		return (
			<table className="table table-striped">
				<thead>
					<tr>
						<th>
							<FormattedMessage id="label.route" />
						</th>
						<th>
							<FormattedMessage id="label.trainSet" />
						</th>
						<th>
							<FormattedMessage id="label.cars" />
						</th>
						<th>
							<FormattedMessage id="label.driver" />
						</th>
						<th>
							<FormattedMessage id="label.phoneNumber" />
						</th>
						<th>
							<FormattedMessage id="label.stationOfDeparture" />
						</th>
						<th>
							<FormattedMessage id="label.timeOfDeparture" />
						</th>
						<th>
							<FormattedMessage id="label.stationOfArrival" />
						</th>
						<th>
							<FormattedMessage id="label.timeOfArrival" />
						</th>
						<th>
							<FormattedMessage id="label.actions" />
						</th>
					</tr>
				</thead>
				{rows}
			</table>
		);
	}
}

TrainsetTable.propTypes = {
	showRoutes: PropTypes.func,
	trainsets: PropTypes.arrayOf(
		PropTypes.shape({
			name: PropTypes.string,
			description: PropTypes.string,
		})
	),
};

export default TrainsetTable;
