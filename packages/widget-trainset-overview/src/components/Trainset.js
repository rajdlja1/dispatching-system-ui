import PropTypes from 'prop-types';
import {
	Train,
	LIFERAY_HOST_PUBLIC_SPACE,
	TRAINSET_DETAIL_PAGE,
	renderVehicle,
} from 'library-ds';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { showTrainsChanged } from '../actions';

const React = require('react');

class Trainset extends React.Component {
	static SHOW_TRAINSETS_LABEL = 'Zobrazit spoje';
	static EDIT_TRAINSET_LABEL = 'Editovat soupravu';

	constructor(props) {
		super(props);
		this.state = { display: true };
	}

	handleShowTrainsChanged = () => {
		this.props.dispatch(showTrainsChanged(this.props.trainset.id));
	};

	render() {
		const { intl } = this.props;
		const trainsetId = this.props.trainset.id;
		const cars = [];
		this.props.trainset.vehicles.forEach(function(car) {
			cars.push(renderVehicle(car, trainsetId));
		});

		const trains = [];

		if (this.props.trainset.trains != null && this.props.trainset.trains.length > 0) {
			this.props.trainset.trains.forEach(function(train) {
				trains.push(<Train key={train.id} train={train} />);
			});
		}

		const link = `${LIFERAY_HOST_PUBLIC_SPACE}/${TRAINSET_DETAIL_PAGE}?id=${trainsetId}`;
		if (this.state.display === false) return null;
		else
			return (
				<tbody>
					<tr key={this.props.trainset.id}>
						<td />
						<td>
							<a href={link}>{this.props.trainset.name}</a>
						</td>
						<td>{cars.map(c => c).reduce((prev, curr) => [prev, ' ', curr])}</td>
						<td />
						<td />
						<td />
						<td />
						<td />
						<td />
						<td>
							<div className="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
								<div className="btn-group mr-2" role="group" aria-label="First group">
									<button
										className="btn btn-info"
										onClick={() => this.handleShowTrainsChanged(trainsetId)}
									>
										{this.props.trainset.showTrains !== false
											? intl.formatMessage({ id: 'label.button.hideTrains' })
											: intl.formatMessage({ id: 'label.button.showTrains' })}
									</button>
								</div>
								<div className="btn-group mr-2" role="group" aria-label="First group" />
							</div>
						</td>
					</tr>
					{this.props.trainset.showTrains !== false && trains}
				</tbody>
			);
	}
}

Trainset.propTypes = {
	showRoutes: PropTypes.func,
	trainset: PropTypes.shape({
		trains: PropTypes.arrayOf(
			PropTypes.shape({
				name: PropTypes.string,
				description: PropTypes.string,
			})
		),
		vehicles: PropTypes.arrayOf(
			PropTypes.shape({
				name: PropTypes.string,
				description: PropTypes.string,
			})
		),
		name: PropTypes.string,
		id: PropTypes.number,
	}),
};

const mapStateToProps = () => {
	return {};
};

export default connect(mapStateToProps)(injectIntl(Trainset));
