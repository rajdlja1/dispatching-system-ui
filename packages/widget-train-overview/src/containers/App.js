import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { fetchItemsIfNeeded } from '../actions';
import renderTrainTable from '../components/TrainTable';
import SearchForm from '../components/SearchForm';

class App extends Component {
	static propTypes = {
		dispatch: PropTypes.func.isRequired,
		isFetching: PropTypes.bool.isRequired,
		trains: PropTypes.array.isRequired,
	};

	componentDidMount() {
		const { dispatch } = this.props;
		dispatch(fetchItemsIfNeeded());
	}

	render() {
		const { trains, isFetching } = this.props;
		const isEmpty = trains.length === 0;
		return (
			<div className="container">
				<div className="jumbotron">
					<div className="row">
						<div className="col-sm-10">
							<h1>
								<FormattedMessage id="label.dashboard" />
							</h1>
						</div>
						<div className="col-sm-2">
							<h2>
								<FormattedMessage id="label.trains" />
							</h2>
						</div>
					</div>
				</div>
				<MuiThemeProvider>
					<SearchForm />
				</MuiThemeProvider>
				<p />
				{isEmpty ? (
					isFetching ? (
						<h2>
							<FormattedMessage id="label.loading" />.
						</h2>
					) : (
						<h2>
							<FormattedMessage id="label.noRecordsFound" />.
						</h2>
					)
				) : (
					<div style={{ opacity: isFetching ? 0.5 : 1 }}>{renderTrainTable(trains)}</div>
				)}
			</div>
		);
	}
}

const mapStateToProps = state => {
	const { filteredTrains } = state;
	const { isFetching, items: trains } = filteredTrains.trains || {
		isFetching: true,
		items: [],
	};

	return {
		trains,
		isFetching,
	};
};

export default connect(mapStateToProps)(App);
