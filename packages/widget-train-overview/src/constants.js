export const LIFERAY_HOST = 'http://localhost:8081';
export const LIFERAY_HOST_PUBLIC_SPACE = 'http://localhost:8081/web/guest';
export const TRAINSET_OVERVIEW_PAGE = 'soupravy';
export const TRAIN_OVERVIEW_PAGE = 'vlaky';
export const TRAINSET_DETAIL_PAGE = 'detail-soupravy';
export const ROUTE_OVERVIEW_PAGE = 'spoje';
export const ROUTE_DETAIL_PAGE = 'detail-spoje';
export const TRAINSET_EDITOR_PAGE = 'editor-soupravy';

export const RideViewModeEnum = { TRAINSET_DETAIL: 1, ROUTE_DETAIL: 2, TRAIN_OVERVIEW: 3 };
