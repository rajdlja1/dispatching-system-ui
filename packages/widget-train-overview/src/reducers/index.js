import { combineReducers } from 'redux';
import {
	SELECT_DATE,
	CHANGE_CHECKBOX_VALUE,
	INVALIDATE_FORM,
	REQUEST_POSTS,
	RECEIVE_POSTS,
	SHOW_ROUTE_SECTIONS_CHANGED,
} from '../actions';

const selectedDate = (state = new Date(), action) => {
	switch (action.type) {
		case SELECT_DATE:
			return action.date;
		default:
			return state;
	}
};

const changedCheckboxValue = (state = 'NOTHING_SELECTED', action) => {
	switch (action.type) {
		case CHANGE_CHECKBOX_VALUE:
			return action.checkboxValue;
		default:
			return state;
	}
};

const trainMan = (
	state = {
		isFetching: false,
		didInvalidate: false,
		items: [],
	},
	action
) => {
	switch (action.type) {
		case INVALIDATE_FORM:
			return {
				...state,
				didInvalidate: true,
			};
		case REQUEST_POSTS:
			return {
				...state,
				isFetching: true,
				didInvalidate: false,
			};
		case RECEIVE_POSTS:
			action.posts.forEach(function(train) {
				train.showRouteSections = false;
			});
			return {
				...state,
				isFetching: false,
				didInvalidate: false,
				items: action.posts,
				lastUpdated: action.receivedAt,
			};
		default:
			return state;
	}
};

const filteredTrains = (state = {}, action) => {
	switch (action.type) {
		case INVALIDATE_FORM:
		case RECEIVE_POSTS:
		case REQUEST_POSTS:
			return {
				...state,
				trains: trainMan(state.trains, action),
			};
		case SHOW_ROUTE_SECTIONS_CHANGED:
			let i;
			let value = false;
			for (i = 0; i < state.trains.items.length; i++) {
				if (state.trains.items[i].id === action.train.id) {
					value = state.trains.items[i].showRouteSections;
					break;
				}
			}
			return {
				...state,
				trains: {
					...state.trains,
					items: state.trains.items.map(
						(e, j) => (j === i ? { ...e, showRouteSections: !value } : e)
					),
				},
			};
		default:
			return state;
	}
};

const rootReducer = combineReducers({
	filteredTrains,
	selectedDate,
	changedCheckboxValue,
});

export default rootReducer;
