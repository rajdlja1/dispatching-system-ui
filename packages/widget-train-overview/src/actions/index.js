/* TODO Change hardcoded URLs */
export const REQUEST_POSTS = 'REQUEST_POSTS';
export const RECEIVE_POSTS = 'RECEIVE_POSTS';
export const INVALIDATE_FORM = 'INVALIDATE_FORM';
export const SELECT_DATE = 'SELECT_DATE';
export const CHANGE_CHECKBOX_VALUE = 'CHANGE_CHECKBOX_VALUE';
export const SHOW_ROUTE_SECTIONS_CHANGED = 'SHOW_ROUTE_SECTIONS_CHANGED';

export const selectDate = date => ({
	type: SELECT_DATE,
	date,
});

export const showRouteSectionsChanged = train => ({
	type: SHOW_ROUTE_SECTIONS_CHANGED,
	train,
});

export const changeCheckBox = checkboxValue => ({
	type: CHANGE_CHECKBOX_VALUE,
	checkboxValue,
});

const fetchPosts = () => dispatch => {
	dispatch(requestPosts());
	return fetch(
		'http://localhost:8080/vlaky/hledej?od=2017-01-01-12:59&do=2018-08-08-12:59&cas=prijezd'
	)
		.then(response => response.json())
		.then(json => dispatch(receivePosts(json)));
};

const reloadItems = (since, until, time) => dispatch => {
	dispatch(requestPosts());
	return fetch(`http://localhost:8080/vlaky/hledej?od=${since}&do=${until}&cas=${time}`)
		.then(response => response.json())
		.then(json => dispatch(receivePosts(json)));
};

export const requestPosts = () => ({
	type: REQUEST_POSTS,
});

export const receivePosts = json => ({
	type: RECEIVE_POSTS,
	posts: json.map(child => child),
	receivedAt: Date.now(),
});

export const invalidateForm = () => ({
	type: INVALIDATE_FORM,
});

const shouldFetchItems = state => {
	const items = state.filteredPosts;
	if (!items) {
		return true;
	}
	if (items.isFetching) {
		return false;
	}
	return items.didInvalidate;
};

export const fetchItemsIfNeeded = () => (dispatch, getState) => {
	if (shouldFetchItems(getState())) {
		return dispatch(fetchPosts());
	}
};

export const reloadItemsIfNeeded = (since, until, time) => (dispatch, getState) => {
	if (shouldFetchItems(getState())) {
		return dispatch(reloadItems(since, until, time));
	}
};
