import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, FormattedDate, FormattedTime, injectIntl, intlShape } from 'react-intl';
import { connect } from 'react-redux';
import { renderVehicle, renderRide } from 'library-ds';
import {
	LIFERAY_HOST_PUBLIC_SPACE,
	ROUTE_DETAIL_PAGE,
	TRAINSET_DETAIL_PAGE,
	RideViewModeEnum,
} from '../constants';

import { showRouteSectionsChanged } from '../actions';

function renderDrivers(train) {
	const driverMap = new Map();
	if (train.rides == null) {
		return (
			<span>
				<FormattedMessage id="label.notAssigned" />
			</span>
		);
	}
	train.rides.forEach(function(ride) {
		if (ride.driver != null) {
			driverMap.set(ride.driver.id, `${ride.driver.firstname} ${ride.driver.lastname}`);
		}
	});
	if (driverMap.size < 1) {
		return (
			<span>
				<FormattedMessage id="label.notAssigned" />
			</span>
		);
	}
	return Array.from(driverMap.values()).toString();
}

function renderContacts(train) {
	const contactMap = new Map();
	if (train.rides == null) {
		return (
			<span>
				<FormattedMessage id="label.notHaveConfiguration" />
			</span>
		);
	}
	train.rides.forEach(function(ride) {
		if (ride.driver != null && ride.driver.phoneNumber != null) {
			contactMap.set(ride.driver.id, ride.driver.phoneNumber);
		}
	});
	if (contactMap.size < 1) {
		return (
			<span>
				<FormattedMessage id="label.notHaveConfiguration" />
			</span>
		);
	}
	return Array.from(contactMap.values()).toString();
}

class Train extends React.Component {
	handleShowRouteSections = () => {
		console.log(this.props.train.showRouteSections);
		this.props.dispatch(showRouteSectionsChanged(this.props.train));
	};

	render() {
		const { intl } = this.props;

		const rides = [];

		const trainId = this.props.train.id;
		if (this.props.train.rides != null && this.props.train.rides.length > 0) {
			this.props.train.rides.forEach(function(ride) {
				rides.push(renderRide(`${trainId}-${ride.id}`, ride, RideViewModeEnum.TRAIN_OVERVIEW));
			});
		}

		const trainsetId = this.props.train.trainSet.id;

		const linkRoute = `${LIFERAY_HOST_PUBLIC_SPACE}/${ROUTE_DETAIL_PAGE}?id=${this.props.train.route.id}`;
		const linkTrainSet = `${LIFERAY_HOST_PUBLIC_SPACE}/${TRAINSET_DETAIL_PAGE}?id=${
			trainsetId
		}`;
		const cars = [];


		this.props.train.trainSet.vehicles.forEach(function(car) {
			cars.push(renderVehicle(car, trainsetId));
		});

		return (
			<tbody>
				<tr key={this.props.train.id} className="trainRow">
					<td>
						<a href={linkRoute}>
							<span className="name">{this.props.train.route.name}</span>
						</a>
					</td>
					<td>
						<a href={linkTrainSet}>{this.props.train.trainSet.name}</a>
					</td>
					<td>{cars.map(c => c).reduce((prev, curr) => [prev, ' ', curr])}</td>
					<td>{renderDrivers(this.props.train)}</td>
					<td>{renderContacts(this.props.train)}</td>
					<td>{this.props.train.route.from.location}</td>
					<td>
						<FormattedDate value={this.props.train.departureTime} />{' '}
						<FormattedTime value={this.props.train.departureTime} />
					</td>
					<td>{this.props.train.route.to.location}</td>
					<td>
						<FormattedDate value={this.props.train.arrivalTime} />{' '}
						<FormattedTime value={this.props.train.arrivalTime} />
					</td>

					<td>
						<div className="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
							<div className="btn-group mr-2" role="group" aria-label="First group">
								<button
									className="btn btn-info"
									onClick={() => this.handleShowRouteSections(this.props.train.id)}
								>
									{this.props.train.showRouteSections !== false
										? intl.formatMessage({ id: 'label.button.hideRouteSections' })
										: intl.formatMessage({ id: 'label.button.showRouteSections' })}
								</button>
							</div>
							<div className="btn-group mr-2" role="group" aria-label="First group" />
						</div>
					</td>
				</tr>
				{this.props.train.showRouteSections !== false && rides}
			</tbody>
		);
	}
}

Train.propTypes = {
	dispatch: PropTypes.func.isRequired,
	intl: intlShape.isRequired,
	route: PropTypes.shape({
		from: PropTypes.shape({
			location: PropTypes.string,
		}),
		name: PropTypes.string,
		to: PropTypes.shape({
			location: PropTypes.string,
		}),
		arrivalTime: PropTypes.string,
		departureTime: PropTypes.string,
		trains: PropTypes.arrayOf(
			PropTypes.shape({
				description: PropTypes.string,
				arrivalTime: PropTypes.string,
				departureTime: PropTypes.string,
			})
		),
		trainset: PropTypes.shape({
			description: PropTypes.number,
		}),
		cars: PropTypes.arrayOf(
			PropTypes.shape({
				name: PropTypes.string,
				description: PropTypes.string,
			})
		),
		id: PropTypes.number,
		showRouteSections: PropTypes.bool,
		rides: PropTypes.array,
		driver: PropTypes.shape({
			id: PropTypes.number,
			firstname: PropTypes.string,
			lastname: PropTypes.string,
			phoneNumber: PropTypes.string,
		}),
		route: PropTypes.shape({
			from: PropTypes.shape({
				location: PropTypes.string,
			}),
			name: PropTypes.string,
			to: PropTypes.shape({
				location: PropTypes.string,
			}),
			departureTime: PropTypes.string,
			arrivalTime: PropTypes.string,
		}),
	}),
	trainset: PropTypes.shape({
		id: PropTypes.number,
	}),
};

const mapStateToProps = () => {
	return {};
};

export default connect(mapStateToProps)(injectIntl(Train));
