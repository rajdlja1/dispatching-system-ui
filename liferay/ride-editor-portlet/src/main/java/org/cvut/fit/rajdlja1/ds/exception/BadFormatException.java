package org.cvut.fit.rajdlja1.ds.exception;

/**
 * Created by JR on 4/4/2018.
 */
public class BadFormatException extends Exception {

    public BadFormatException(String message) {
        super(message);
    }
}
