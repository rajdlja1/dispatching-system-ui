package org.cvut.fit.rajdlja1.ds.constants;

/**
 * Portlet Keys
 *
 * @author Jan Rajdl
 */
public class RideEditorPortletKeys {

	public static final String RIDE_EDITOR = "org_cvut_fit_rajdlja1_ds_portlet_Ride_Editor";
	public static final String CONFIGURATION = "RideEditorConfiguration";

}
