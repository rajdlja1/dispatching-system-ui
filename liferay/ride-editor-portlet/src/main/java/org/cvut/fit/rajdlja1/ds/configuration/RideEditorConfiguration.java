package org.cvut.fit.rajdlja1.ds.configuration;

import aQute.bnd.annotation.metatype.Meta;
import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;
import org.cvut.fit.rajdlja1.ds.constants.RideEditorPortletKeys;

/**
 * Configuration class of the Ride Editor portlet.
 *
 * @author Jan Rajdl
 */
@ExtendedObjectClassDefinition(
        category = "foundation",
        scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE
)
@Meta.OCD(id = RideEditorPortletKeys.CONFIGURATION)
public interface RideEditorConfiguration {

    @Meta.AD(
            required = true
    )
    String heading();

    @Meta.AD(
            required = true
    )
    String content();

}
