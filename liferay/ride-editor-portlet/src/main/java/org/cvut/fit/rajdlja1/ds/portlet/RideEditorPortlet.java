package org.cvut.fit.rajdlja1.ds.portlet;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import org.cvut.fit.rajdlja1.ds.configuration.RideEditorConfigurationUtil;
import org.cvut.fit.rajdlja1.ds.constants.RideEditorPortletKeys;
import com.liferay.portal.kernel.util.PortalUtil;
import org.cvut.fit.rajdlja1.ds.exception.BadFormatException;
import org.osgi.service.component.annotations.Component;

/**
 * Controller of Ride Editor Portlet, it renders view.jsp with the React widgets.
 *
 * @author Jan Rajdl
 */
@Component(
	configurationPid = RideEditorPortletKeys.CONFIGURATION,
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + RideEditorPortletKeys.RIDE_EDITOR,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)

public class RideEditorPortlet extends MVCPortlet {

	private static SimpleDateFormat DATE_TIME_FORMATTER = new SimpleDateFormat("yyyy-MM-dd-h:m");
	private static final String DATE_BAD_FORMAT = "Datum v url adrese musí splňovat následující formát yyyy-MM-dd-h:m, např.: 2018-04-01-9:30.";

	public static Date validateDateFromUrl(String strDate) throws BadFormatException {
		Date date = null;
		try {
			date = DATE_TIME_FORMATTER.parse(strDate);
		} catch (ParseException e) {
			throw new BadFormatException(DATE_BAD_FORMAT);
		}
		return date;
	}

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		try {
			RideEditorConfigurationUtil.addConfigurationContext(renderRequest);
		} catch (ConfigurationException e) {
			throw new PortletException("Configuration error", e);
		}

        HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
		String id = PortalUtil.getOriginalServletRequest(request).getParameter("id");
		Integer numericId = Integer.parseInt(id);


		String fromParam = PortalUtil.getOriginalServletRequest(request).getParameter("od");
		String toParam = PortalUtil.getOriginalServletRequest(request).getParameter("do");
		String depotId = PortalUtil.getOriginalServletRequest(request).getParameter("depotId");

		if (fromParam != null && toParam != null) {
			try {
				validateDateFromUrl(fromParam);
				validateDateFromUrl(toParam);
			} catch (BadFormatException e) {
				renderRequest.setAttribute("error", "Bad Format of Date Parameter");
				super.render(renderRequest, renderResponse);
			}
		} else {
			renderRequest.setAttribute("error", "Missing Date Parameter");
		}

		renderRequest.setAttribute("from", fromParam);
		renderRequest.setAttribute("to", toParam);
		renderRequest.setAttribute("id", id);
		renderRequest.setAttribute("depotId", depotId);

		super.render(renderRequest, renderResponse);
	}

}

