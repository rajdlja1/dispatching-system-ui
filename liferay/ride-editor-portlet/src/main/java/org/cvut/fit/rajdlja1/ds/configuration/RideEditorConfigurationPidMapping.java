package org.cvut.fit.rajdlja1.ds.configuration;

import com.liferay.portal.kernel.settings.definition.ConfigurationPidMapping;
import org.cvut.fit.rajdlja1.ds.constants.RideEditorPortletKeys;
import org.osgi.service.component.annotations.Component;

/**
 * @author Jan Rajdl
 */
@Component
public class RideEditorConfigurationPidMapping implements ConfigurationPidMapping {

    @Override
    public Class<?> getConfigurationBeanClass() {
        return RideEditorConfiguration.class;
    }

    @Override
    public String getConfigurationPid() {
        return RideEditorPortletKeys.RIDE_EDITOR;
    }
}
