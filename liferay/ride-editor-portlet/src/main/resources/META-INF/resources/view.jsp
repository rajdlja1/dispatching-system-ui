<%--
Renders React widgets.
--%>
<%@ include file="init.jsp" %>

<%--suppress JSUnresolvedVariable, JSUnresolvedFunction --%>
<script type="text/javascript">
    Liferay.Loader.require("app-demo");
</script>

<div id="header"></div>
<script data-union-widget="header" data-union-container="header" type="application/json"></script>

<div id="trainset-editor"></div>
<script data-union-widget="trainset-editor" data-union-container="trainset-editor" type="application/json">
    {
    "id": "${id}",
    "from": "${from}",
    "to": "${to}",
    "depotId": "${depotId}"
    }
</script>
