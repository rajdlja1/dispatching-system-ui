package org.cvut.fit.rajdlja1.ds.constants;

/**
 * Portlet Keys
 *
 * @author Jan Rajdl
 */
public class RouteOverviewPortletKeys {

	public static final String ROUTE_OVERVIEW = "org_cvut_fit_rajdlja1_ds_portlet_Route_Overview";
	public static final String CONFIGURATION = "RouteOverviewConfiguration";

}
