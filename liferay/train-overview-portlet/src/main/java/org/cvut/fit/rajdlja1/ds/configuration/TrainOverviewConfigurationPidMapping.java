package org.cvut.fit.rajdlja1.ds.configuration;

import com.liferay.portal.kernel.settings.definition.ConfigurationPidMapping;
import org.cvut.fit.rajdlja1.ds.constants.TrainOverviewPortletKeys;
import org.osgi.service.component.annotations.Component;

/**
 * @author Jan Rajdl (rajdlja1@fit.cvut.cz)
 */
@Component
public class TrainOverviewConfigurationPidMapping implements ConfigurationPidMapping {

    @Override
    public Class<?> getConfigurationBeanClass() {
        return TrainOverviewConfiguration.class;
    }

    @Override
    public String getConfigurationPid() {
        return TrainOverviewPortletKeys.TRAIN_OVERVIEW;
    }
}
