package org.cvut.fit.rajdlja1.ds.constants;

/**
 * Train Overview widget's constants.
 *
 * @author Jan Rajdl (rajdlja1@fit.cvut.cz)
 */
public class TrainOverviewConstants {

    public static final String ATTR_HEADING = "heading";
    public static final String ATTR_CONTENT = "content";

    public static final String PARAM_HEADING = "heading";
    public static final String PARAM_CONTENT = "content";

}
