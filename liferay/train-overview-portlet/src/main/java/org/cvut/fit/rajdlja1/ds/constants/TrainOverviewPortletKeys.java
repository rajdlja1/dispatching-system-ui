package org.cvut.fit.rajdlja1.ds.constants;

/**
 * Portlet Keys
 *
 * @author Jan Rajdl (rajdlja1@fit.cvut.cz)
 */
public class TrainOverviewPortletKeys {

	public static final String TRAIN_OVERVIEW = "org_cvut_fit_rajdlja1_ds_portlet_TrainOverview";
	public static final String CONFIGURATION = "TrainOverviewConfiguration";

}