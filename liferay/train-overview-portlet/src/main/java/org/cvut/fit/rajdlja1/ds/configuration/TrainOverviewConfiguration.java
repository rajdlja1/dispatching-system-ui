package org.cvut.fit.rajdlja1.ds.configuration;

import aQute.bnd.annotation.metatype.Meta;
import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;
import org.cvut.fit.rajdlja1.ds.constants.TrainOverviewPortletKeys;

/**
 * Configuration class of the Train Overview portlet.
 *
 * @author Jan Rajdl (rajdlja1@fit.cvut.cz)
 */
@ExtendedObjectClassDefinition(
        category = "foundation",
        scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE
)
@Meta.OCD(id = TrainOverviewPortletKeys.CONFIGURATION)
public interface TrainOverviewConfiguration {

    @Meta.AD(
            required = true,
            deflt = ""
    )
    String heading();

    @Meta.AD(
            required = true,
            deflt = ""
    )
    String content();

}
