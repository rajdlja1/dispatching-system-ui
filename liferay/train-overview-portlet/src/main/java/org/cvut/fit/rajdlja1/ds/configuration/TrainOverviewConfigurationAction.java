package org.cvut.fit.rajdlja1.ds.configuration;

import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import org.cvut.fit.rajdlja1.ds.constants.TrainOverviewPortletKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;

/**
 * The configuration action class enables Configuration item in the portlet's menu with gear icon.
 * Configuration.jsp is used to render the UI.
 *
 * @author Jan Rajdl (rajdlja1@fit.cvut.cz)
 */
@Component(
    configurationPid = TrainOverviewPortletKeys.CONFIGURATION,
    configurationPolicy = ConfigurationPolicy.OPTIONAL,
    immediate = true,
    property = {
        "javax.portlet.name=" + TrainOverviewPortletKeys.TRAIN_OVERVIEW,
    },
    service = ConfigurationAction.class
)
public class TrainOverviewConfigurationAction extends DefaultConfigurationAction {

    @Override
    public void include(PortletConfig portletConfig, HttpServletRequest request, HttpServletResponse response) throws Exception {
        TrainOverviewConfigurationUtil.addConfigurationContext(request);

        super.include(portletConfig, request, response);
    }

}
