<%--
Renders React widgets.
--%>
<%@ include file="./init.jsp" %>

<%--suppress JSUnresolvedVariable, JSUnresolvedFunction --%>
<script type="text/javascript">
	Liferay.Loader.require("app-demo");
</script>

<div id="header"></div>
<script data-union-widget="header" data-union-container="header" type="application/json"></script>

<div id="${ns}train-overview"></div>
<script data-union-widget="train-overview" data-union-container="${ns}train-overview" type="application/json"></script>
