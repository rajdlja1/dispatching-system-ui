package org.cvut.fit.rajdlja1.ds.constants;

/**
 * Portlet Keys
 *
 * @author Jan Rajdl
 */
public class RouteDetailPortletKeys {

	public static final String ROUTE_DETAIL = "org_cvut_fit_rajdlja1_ds_portlet_Route_Detail";
	public static final String CONFIGURATION = "RouteDetailConfiguration";

}
