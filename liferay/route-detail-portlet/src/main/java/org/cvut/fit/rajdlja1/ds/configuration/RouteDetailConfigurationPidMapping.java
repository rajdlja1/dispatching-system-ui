package org.cvut.fit.rajdlja1.ds.configuration;

import com.liferay.portal.kernel.settings.definition.ConfigurationPidMapping;
import org.cvut.fit.rajdlja1.ds.constants.RouteDetailPortletKeys;
import org.osgi.service.component.annotations.Component;

/**
 * @author Jan Rajdl
 */
@Component
public class RouteDetailConfigurationPidMapping implements ConfigurationPidMapping {

    @Override
    public Class<?> getConfigurationBeanClass() {
        return RouteDetailConfiguration.class;
    }

    @Override
    public String getConfigurationPid() {
        return RouteDetailPortletKeys.ROUTE_DETAIL;
    }
}
