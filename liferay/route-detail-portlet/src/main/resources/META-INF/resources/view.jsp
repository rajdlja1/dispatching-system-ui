<%--
Renders React widgets.
--%>
<%@ include file="init.jsp" %>

<%--suppress JSUnresolvedVariable, JSUnresolvedFunction --%>
<script type="text/javascript">
    Liferay.Loader.require("app-demo");
</script>

    <div id="header"></div>
    <script data-union-widget="header" data-union-container="header" type="application/json"></script>

    <div id="route-detail"></div>
    <script data-union-widget="route-detail" data-union-container="route-detail" type="application/json">
    {
    "id": "${id}"
    }
    </script>

