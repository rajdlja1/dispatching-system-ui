<%--
Renders React widgets.
--%>
<%@ include file="init.jsp" %>

<%--suppress JSUnresolvedVariable, JSUnresolvedFunction --%>
<script type="text/javascript">
    Liferay.Loader.require("app-demo");
</script>

<div id="header"></div>
<script data-union-widget="header" data-union-container="header" type="application/json"></script>

<div id="trainset-overview"></div>
<script data-union-widget="trainset-overview" data-union-container="trainset-overview" type="application/json"></script>
