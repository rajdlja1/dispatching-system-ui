package org.cvut.fit.rajdlja1.ds.constants;

/**
 * Trainset Overview widget's constants.
 *
 * @author Jan Rajdl
 */
public class TrainsetOverviewConstants {

    public static final String ATTR_HEADING = "heading";
    public static final String ATTR_CONTENT = "content";

    public static final String PARAM_HEADING = "heading";
    public static final String PARAM_CONTENT = "content";

}
