package org.cvut.fit.rajdlja1.ds.constants;

/**
 * Portlet Keys
 *
 * @author Jan Rajdl
 */
public class TrainsetOverviewPortletKeys {

	public static final String TRAINSET_OVERVIEW = "org_cvut_fit_rajdlja1_ds_portlet_Trainset_Overview";
	public static final String CONFIGURATION = "TrainsetOverviewConfiguration";

}
