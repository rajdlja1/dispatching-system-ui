package org.cvut.fit.rajdlja1.ds.configuration;

import com.liferay.portal.kernel.settings.definition.ConfigurationPidMapping;
import org.cvut.fit.rajdlja1.ds.constants.TrainsetOverviewPortletKeys;
import org.osgi.service.component.annotations.Component;

/**
 * @author Jan Rajdl
 */
@Component
public class TrainsetOverviewConfigurationPidMapping implements ConfigurationPidMapping {

    @Override
    public Class<?> getConfigurationBeanClass() {
        return TrainsetOverviewConfiguration.class;
    }

    @Override
    public String getConfigurationPid() {
        return TrainsetOverviewPortletKeys.TRAINSET_OVERVIEW;
    }
}
