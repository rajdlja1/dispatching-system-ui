package org.cvut.fit.rajdlja1.ds.configuration;

import aQute.bnd.annotation.metatype.Meta;
import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;
import org.cvut.fit.rajdlja1.ds.constants.TrainsetOverviewPortletKeys;

/**
 * Configuration class of the Trainset Overview portlet.
 *
 * @author Jan Rajdl
 */
@ExtendedObjectClassDefinition(
        category = "foundation",
        scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE
)
@Meta.OCD(id = TrainsetOverviewPortletKeys.CONFIGURATION)
public interface TrainsetOverviewConfiguration {

    @Meta.AD(
            required = true
    )
    String heading();

    @Meta.AD(
            required = true
    )
    String content();

}
