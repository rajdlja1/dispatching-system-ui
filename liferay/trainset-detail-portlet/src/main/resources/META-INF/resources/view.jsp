<%--
Renders React widgets.
--%>
<%@ include file="init.jsp" %>

<%--suppress JSUnresolvedVariable, JSUnresolvedFunction --%>
<script type="text/javascript">
    Liferay.Loader.require("app-demo");
</script>

<div id="header"></div>
<script data-union-widget="header" data-union-container="header" type="application/json"></script>

<div id="trainset-detail"></div>
<script data-union-widget="trainset-detail" data-union-container="trainset-detail" type="application/json">
    {
    "id": "${id}"
    }
</script>
