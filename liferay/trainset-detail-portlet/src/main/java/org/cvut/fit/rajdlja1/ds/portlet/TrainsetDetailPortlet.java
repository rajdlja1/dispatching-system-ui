package org.cvut.fit.rajdlja1.ds.portlet;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import org.cvut.fit.rajdlja1.ds.configuration.TrainsetDetailConfigurationUtil;
import org.cvut.fit.rajdlja1.ds.constants.TrainsetDetailPortletKeys;
import com.liferay.portal.kernel.util.PortalUtil;
import org.osgi.service.component.annotations.Component;

/**
 * Controller of Trainset Detail Portlet, it renders view.jsp with the React widgets.
 *
 * @author Jan Rajdl
 */
@Component(
	configurationPid = TrainsetDetailPortletKeys.CONFIGURATION,
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + TrainsetDetailPortletKeys.TRAINSET_DETAIL,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class TrainsetDetailPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		try {
			TrainsetDetailConfigurationUtil.addConfigurationContext(renderRequest);
		} catch (ConfigurationException e) {
			throw new PortletException("Configuration error", e);
		}

		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
		String id = PortalUtil.getOriginalServletRequest(request).getParameter("id");
		Integer numericId = Integer.parseInt(id);
		renderRequest.setAttribute("id", id);

		super.render(renderRequest, renderResponse);
	}

}
