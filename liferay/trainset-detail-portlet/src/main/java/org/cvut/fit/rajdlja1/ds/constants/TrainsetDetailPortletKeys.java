package org.cvut.fit.rajdlja1.ds.constants;

/**
 * Portlet Keys
 *
 * @author Jan Rajdl
 */
public class TrainsetDetailPortletKeys {

	public static final String TRAINSET_DETAIL = "org_cvut_fit_rajdlja1_ds_portlet_Trainset_Detail";
	public static final String CONFIGURATION = "TrainsetDetailConfiguration";

}
