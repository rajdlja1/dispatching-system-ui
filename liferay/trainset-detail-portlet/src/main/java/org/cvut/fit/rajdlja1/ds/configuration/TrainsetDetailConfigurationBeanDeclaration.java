package org.cvut.fit.rajdlja1.ds.configuration;

import com.liferay.portal.kernel.settings.definition.ConfigurationBeanDeclaration;
import org.osgi.service.component.annotations.Component;

/**
 * Registers the configuration class {@link TrainsetDetailConfiguration}. It enables the system to keep track of any
 * configuration changes as they happen.
 *
 * @author Jan Rajdl
 */
@Component
public class TrainsetDetailConfigurationBeanDeclaration implements ConfigurationBeanDeclaration {

    /**
     * Returns configuration class.
     *
     * @return configuration class
     */
    @Override
    public Class<?> getConfigurationBeanClass() {
        return TrainsetDetailConfiguration.class;
    }
}
