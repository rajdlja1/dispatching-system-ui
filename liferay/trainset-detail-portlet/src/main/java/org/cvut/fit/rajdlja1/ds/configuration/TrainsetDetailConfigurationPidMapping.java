package org.cvut.fit.rajdlja1.ds.configuration;

import com.liferay.portal.kernel.settings.definition.ConfigurationPidMapping;
import org.cvut.fit.rajdlja1.ds.constants.TrainsetDetailPortletKeys;
import org.osgi.service.component.annotations.Component;

/**
 * @author Jan Rajdl
 */
@Component
public class TrainsetDetailConfigurationPidMapping implements ConfigurationPidMapping {

    @Override
    public Class<?> getConfigurationBeanClass() {
        return TrainsetDetailConfiguration.class;
    }

    @Override
    public String getConfigurationPid() {
        return TrainsetDetailPortletKeys.TRAINSET_DETAIL;
    }
}
